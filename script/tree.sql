--
-- Tábla szerkezet: `tree`
--

CREATE TABLE IF NOT EXISTS `tree` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- A tábla adatainak kiíratása `tree`
--

INSERT INTO `tree` (`id`, `label`, `parent_id`) VALUES
(1, 'Food', 0),
(2, 'Fruit', 1),
(3, 'Meat', 1),
(4, 'Red', 2),
(5, 'Yellow', 2),
(6, 'Beef', 3),
(7, 'Pork', 3),
(8, 'Cherry', 4),
(9, 'Banana', 5);