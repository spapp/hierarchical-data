<?php
// FIXME
//error_reporting(E_ALL | E_STRICT);
//ini_set('display_errors', 1);
/**
 * Define path to application directory.
 *
 * @global string
 */
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'application'));

/**
 * Autoloader initialization.
 */
require_once(dirname(APPLICATION_PATH) . DIRECTORY_SEPARATOR . 'library' . DIRECTORY_SEPARATOR . 'Loader.php');
Loader::initAutoLoader(dirname(APPLICATION_PATH) . DIRECTORY_SEPARATOR . 'library', APPLICATION_PATH);
/**
 * Run application.
 */
Application::getInstance(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'configs')->run();
