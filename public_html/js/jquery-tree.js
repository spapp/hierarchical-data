/**
 * tree jQuery plugin.
 *
 * @author Sandor Papp <spapp@spappsite.hu>
 * @copyright Copyright (c) 2013, Sandor Papp
 * @license http://creativecommons.org/licenses/by-nc-nd/3.0/deed.hu Creative Commons
 */
// TODO not complete, needs refactor
(function ($) {
    $.fn.tree = function (aOptions) {
        var options = $.extend({}, $.fn.tree.defaults, aOptions),
            tree = $(this);

        function getSelected() {
            return tree.find('.' + options.selectedClass);
        }

        function getChildren(parent) {
            options.getChildren(getParentId(parent), function (data) {
                appendChildren(parent, data);
            });
        }

        function getParentId(node) {
            var parentId = null;

            if (node) {
                if ($(node)[0].tagName != 'LI') {
                    parentId = node.parent().attr('id');
                } else {
                    parentId = node.attr('id');
                }
            }

            if (parentId) {
                parentId = parentId.split('-');
                parentId = parseInt(parentId.pop(), 10);
            }

            return parentId;
        }

        function appendChildren(parent, children) {
            var ol = [], parentId = getParentId(parent) || 0;

            if (!parentId) {
                parent = tree;
            } else if (parent[0].tagName != 'LI') {
                parent = parent.parent();
            }

            for (var i = 0; i < children.length; i++) {
                ol.push('<li id="', tree.attr('id'), '-node-', children[i].id, '" class="', options.closeClass, '">',
                        '<img alt="" class="node" src="', options.emptyImage, '">',
                        '<a href="#">', children[i].label, '</a>',
                        '<ol>', '</ol>',
                        '</li>');

            }

            parent.children('ol').html(ol.join(''));
            parent.removeClass(options.closeClass).addClass(options.openClass);
        }

        $(tree).click(function (event) {
            event.preventDefault();
            var target = $(event.target);

            if (target[0].tagName != 'LI') {
                target = target.parent();
            }

            getSelected().removeClass(options.selectedClass);
            target.addClass(options.selectedClass);

            if (target.hasClass(options.openClass)) {
                target.find('> ol > li').remove();
                target.removeClass(options.openClass).addClass(options.closeClass);
            } else {
                getChildren(getSelected());
            }

            if (getSelected().length) {
                $(options.addButton).removeAttr('disabled');
                $(options.deleteButton).removeAttr('disabled');
                $(options.editButton).removeAttr('disabled');
            } else {
                $(options.addButton).attr('disabled', 'disabled');
                $(options.deleteButton).attr('disabled', 'disabled');
                $(options.editButton).attr('disabled', 'disabled');
            }
        });
        $(options.addButton).click(function () {
            event.preventDefault();

            var label = window.prompt(options.addText, ''),
                selected = getSelected();

            if (label) {
                options.addNode({
                                    label: label,
                                    parent: selected.length ? getParentId(selected) : 0
                                }, function (data) {
                    if (data) {
                        getChildren(selected.length ? selected : tree);
                    }

                });
            }
        });
        $(options.deleteButton).click(function () {
            event.preventDefault();
            var selected = getSelected();

            if (selected.length && window.confirm(options.deleteText + ' ' + selected.children('a').html())) {
                options.deleteNode(getParentId(selected), function (data) {
                    if (data) {
                        selected.remove();
                    }
                });
            }

        });
        $(options.editButton).click(function () {
            event.preventDefault();
            var selected = getSelected(),
                oldLabel = selected.children('a').html();

            var newLabel = window.prompt(options.editText, oldLabel);

            if (oldLabel != newLabel) {
                options.setLabel({
                                     oldLabel: oldLabel,
                                     newLabel: newLabel,
                                     id: getParentId(selected)
                                 }, function (data) {
                    if (data) {
                        selected.children('a').html(newLabel);
                    }

                });
            }
        });

        tree.html('<ol></ol>');

        getChildren();
    };

    $.fn.tree.defaults = {
        getChildren: function (parent, aCallback) {
            var data = {
                c: 'tree',
                a: 'children'
            };

            if (parent > 0) {
                data.parent = parent;
            }

            $.ajax({type: 'GET', url: 'index.php', data: data}).done(aCallback);
        },
        setLabel: function (node, aCallback) {
            var data = {
                c: 'tree',
                a: 'label',
                id: node.id,
                label: node.newLabel
            };

            $.ajax({type: 'POST', url: 'index.php', data: data }).done(aCallback);
        },
        addNode: function (node, aCallback) {
            var url = 'index.php?c=tree&a=add&parent=' + node.parent + '&label=' + node.label;
            $.ajax({type: 'PUT', url: url, data: {} }).done(aCallback);
        },
        deleteNode: function (id, aCallback) {
            var url = 'index.php?c=tree&a=delete&id=' + id;
            $.ajax({type: 'DELETE', url: url, data: {} }).done(aCallback);
        },
        selectedClass: 'selected',
        openClass: 'open',
        closeClass: 'close',
        emptyImage: 'skin/images/e.png',
        addButton: '#tree-node-add',
        deleteButton: '#tree-node-delete',
        editButton: '#tree-node-edit',
        editText: 'Új felirat',
        addText: 'Új node',
        deleteText: 'Biztos, hogy törölni akarod:'
    };

})(jQuery);