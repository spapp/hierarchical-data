<?php
/**
 * Created: 2013.04.13. 21:51
 *
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright Copyright (c) 2013, Sandor Papp
 * @package   hierarchical_data
 * @license   http://creativecommons.org/licenses/by-nc-nd/3.0/deed.hu
 */

class Model_Mapper_Tree {
    protected $dbTable = null;
    /**
     * @var Model_Mapper_Tree
     */
    protected static $instance = null;

    protected function __construct() {
    }

    /**
     * @param array $data
     *
     * @return int|mixed
     */
    public function addNode($data) {
        $row = $this->getTable()->createRow($data);
        return $row->save();
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function delete($id) {
        $row = $this->getTable()->getNodeById($id);
        $children = $this->getChildren($row->id);

        if ($children) {
            foreach ($children as $child) {
                $this->delete($child->id);
            }
        }

        return $row->delete();
    }

    /**
     * @param int $id
     * @param string $label
     *
     * @return mixed
     */
    public function setLabel($id, $label) {
        $row = $this->getTable()->getNodeById($id);
        $row->label = $label;

        return $row->save();
    }

    /**
     * @param null|int $parentId
     *
     * @return mixed
     */
    public function getChildren($parentId = null) {
        if (!$parentId) {
            return $this->getTable()->getRootNode();
        }

        $parent = $this->getTable()->getNodeById($parentId);
        return $this->getTable()->getChildren($parent);
    }

    /**
     * @return Model_DbTable_Tree|null
     */
    public function getTable() {
        if (null === $this->dbTable) {
            $this->dbTable = new Model_DbTable_Tree();
        }
        return $this->dbTable;
    }

    /**
     * @return Model_Mapper_Tree|null
     */
    public static function getInstance() {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

}
