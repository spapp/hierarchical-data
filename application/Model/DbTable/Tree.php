<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright Copyright (c) 2013, Sandor Papp
 * @package   hierarchical_data
 * @license   http://creativecommons.org/licenses/by-nc-nd/3.0/deed.hu
 */

class Model_DbTable_Tree extends Db_Table {
    /**
     * @var string
     */
    protected $name = 'tree';

    /**
     * @return mixed
     */
    public function getRootNode() {
        return $this->getChildren();
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function getNodeById($id) {
        $node = $this->select('id = ' . Db::PARAM_TOKEN . 'id', '*', array('id' => $id));
        return $node->current();
    }

    /**
     * @param null $node
     *
     * @return mixed
     */
    public function getChildren($node = null) {
        $parentId = 0;
        if ($node) {
            $parentId = $node->id;
        }
        return $this->select('parent_id = ' . Db::PARAM_TOKEN . 'parent_id', '*', array('parent_id' => $parentId));
    }
}
