<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright Copyright (c) 2013, Sandor Papp
 * @package   hierarchical_data
 * @license
 */

class TreeController extends Controller {
    public function init() {
        if ('default' != $this->getRequest()->getActionName()) {
            $this->getLayout()->setRenderType(Layout::RENDER_TYPE_JSON);
        }
    }

    public function defaultAction() {

    }

    public function childrenAction() {
        if ($this->getRequest()->isGet()) {
            $children = $this->getMapper('tree')->getChildren($this->getRequest()->getGet('parent', null));

            if ($children) {
                $this->setData($children->toArray());
            }
        }
    }

    public function addAction() {
        if ($this->getRequest()->getMethod() == Request::METHOD_PUT) {
            $parent = $this->getRequest()->getParam('parent', null);
            $label = $this->getRequest()->getParam('label', null);

            $this->setData(
                $this->getMapper('tree')->addNode(
                    array(
                         'parent_id' => $parent,
                         'label' => $label
                    )
                )
            );
        }
    }

    public function labelAction() {
        if ($this->getRequest()->isPost()) {
            $id = $this->getRequest()->getPost('id', null);
            $label = $this->getRequest()->getPost('label', null);
            $this->setData($this->getMapper('tree')->setLabel($id, $label));
        }
    }

    public function deleteAction() {
        if ($this->getRequest()->getMethod() == Request::METHOD_DELETE) {
            $id = $this->getRequest()->getParam('id', null);
            $this->setData($this->getMapper('tree')->delete($id));
        }
    }
}