<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright Copyright (c) 2013, Sandor Papp
 * @package   hierarchical_data
 * @license
 */

class DefaultController extends Controller {
    public function init() {
    }

    public function defaultAction() {
        $this->getView()->set('text', 'Loren ipsum');

        $table = new Db_Table('test');

        $this->getView()->set('test', $table->select());
    }
}