<?php
/**
 * Class Db
 * For connecting to SQL databases and storage it.
 */
class Db {
    const PARAM_TOKEN = '%%';
    /**
     *  Result row fetch modes.
     */
    const FETCH_MODE_ASSOC = 0;
    const FETCH_MODE_OBJECT = 1;
    /**
     * @var Db
     */
    protected static $instance = null;
    /**
     * @var string
     */
    protected $defaultAdapter = null;
    /**
     * @var null|Array
     */
    protected $defaultAdapterConfig = null;
    /**
     * @var array|null
     */
    protected $dbs = null;

    /**
     * Constructor.
     */
    protected function __construct() {
        $this->dbs = array();
    }

    /**
     * Add some database adapter.
     *
     * @param array|Config$config
     *
     * @return $this
     */
    public function addDbAdapters($config) {
        foreach ($config as $name => $dbConfig) {
            $this->addDbAdapter($name, $dbConfig);
        }
        return $this;
    }

    /**
     * Add a database adapter.
     *
     * @param string $name
     * @param array|Config $config
     *
     * @return $this
     */
    public function addDbAdapter($name, $config) {
        if ($config instanceof Config) {
            $config = $config->toArray();
        }
        if (is_array($this->defaultAdapterConfig)) {
            $config = array_merge($this->defaultAdapterConfig, $config);
        }

        if ((isset($config['default']) and true == $config['default']) or null === self::getDefaultAdapter()) {
            $this->defaultAdapterConfig = $config;
            self::setDefaultAdapter($name);
        }

        $this->dbs[$name] = self::factory($config);

        return $this;
    }

    /**
     * Returns db object instance.
     *
     * @return Application|null
     */
    public static function getInstance() {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Factory for Db_Adapter_<adapter> classes.
     *
     * Second argument is optional. This is used as the argument to the adapter constructor.
     *      array (
     *          'adapter'   => 'mysqli',
     *          'default'   => true,
     *          'host'      => 'localhost,
     *          'username'  => '',
     *          'password'  => '',
     *          'dbname'    => '',
     *          'port'      => 3306,
     *          'charset'   => 'utf8',
     *          'fetchMode' => Db::FETCH_MODE_OBJECT
     *      );
     *
     * If the first argument is of type Config or array, it is assumed to contain
     * all parameters, and the second argument is ignored.
     *
     * @param string $adapter
     * @param null|array|Config $config
     *
     * @return Db_Adapter_Abstract
     * @throws Db_Exception
     */
    public static function factory($adapter, $config = null) {
        if ($adapter instanceof Config) {
            $config = $adapter->toArray();
        } elseif (is_array($adapter)) {
            $config = $adapter;
        } elseif (!$config) {
            throw new Db_Exception('Not supported parameter type.');
        }

        if (!is_string($adapter)) {
            $adapter = $config['adapter'];
        }

        $class = 'Db_Adapter_' . ucfirst(strtolower($adapter));
        return new $class($config);
    }

    /**
     * Returns a Db_Adapter_Abstract.
     * If name is null returns the default adapter.
     *
     * @param null|string $name
     *
     * @return null|Db_Adapter_Abstract
     */
    public static function getAdapter($name = null) {
        if (null === $name) {
            $name = self::getInstance()->defaultAdapter;
        }
        return self::getInstance()->dbs[$name];
    }

    /**
     * Returns the default database adapter.
     *
     * @return Db_Adapter_Abstract|null
     */
    public static function getDefaultAdapter() {
        return self::getAdapter();
    }

    /**
     * Set the default database adapter name.
     *
     * @param string $name
     */
    public static function setDefaultAdapter($name) {
        self::getInstance()->defaultAdapter = $name;
    }
}
