<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright Copyright (c) 2013, Sandor Papp
 * @package   hierarchical_data
 * @license
 */
class Request {
    /**
     * HTTP methods
     */
    const METHOD_OPTIONS = 'OPTIONS';

    const METHOD_GET = 'GET';

    const METHOD_HEAD = 'HEAD';

    const METHOD_POST = 'POST';

    const METHOD_PUT = 'PUT';

    const METHOD_DELETE = 'DELETE';

    const METHOD_TRACE = 'TRACE';

    const METHOD_CONNECT = 'CONNECT';

    const DEFAULT_CONTROLLER = 'default';
    const DEFAULT_ACTION = 'default';
    const DEFAULT_CONTROLLER_KEY = 'c';
    const DEFAULT_ACTION_KEY = 'a';
    /**
     * @var array|null
     */
    protected $params = null;
    /**
     * @var Config|null
     */
    protected $config = null;

    /**
     * Constructor.
     *
     * @param Config $config
     */
    public function __construct(Config $config = null) {
        $this->params = array(
            self::METHOD_GET => $_GET,
            self::METHOD_POST => $_POST
        );
        $this->config = $config;
    }

    /**
     * Returns all post and get params.
     *
     * @return array
     */
    public function getParams() {
        return array_merge($this->params[self::METHOD_GET], $this->params[self::METHOD_POST]);
    }

    /**
     * Returns a parameter value.
     *
     * @param string $name   param name
     * @param mixed $default default value
     *
     * @return mixed|null
     */
    public function getParam($name, $default = null) {
        $return = $this->getGet($name);
        if (null === $return) {
            $return = $this->getPost($name, $default);
        }

        return $return;
    }

    /**
     * Set a parameter value.
     *
     * @param string $name   param name
     * @param mixed $value   value
     *
     * @return $this
     */
    public function setParam($name, $value) {
        $this->params[self::METHOD_GET][$name] = $value;

        return $this;
    }

    /**
     * Returns a GET parameter value.
     *
     * @param string $name   param name
     * @param mixed $default default value
     *
     * @return mixed|null
     */
    public function getGet($name, $default = null) {
        if (isset($this->params[self::METHOD_GET][$name])) {
            return $this->params[self::METHOD_GET][$name];
        }

        return $default;
    }

    /**
     * Returns a POST parameter value.
     *
     * @param string $name   param name
     * @param mixed $default default value
     *
     * @return mixed|null
     */
    public function getPost($name, $default = null) {
        if (isset($this->params[self::METHOD_POST][$name])) {
            return $this->params[self::METHOD_POST][$name];
        }

        return $default;
    }

    /**
     * Returns http request method.
     *
     * @see self::METHOD_<method name>
     * @return string
     */
    public function getMethod() {
        return Application::getInstance()->getServer('REQUEST_METHOD');
    }

    /**
     * Returns true if current request is POST.
     *
     * @return bool
     */
    public function isPost() {
        return $this->getMethod() === self::METHOD_POST;
    }

    /**
     * Returns true if current request is GET.
     *
     * @return bool
     */
    public function isGet() {
        return $this->getMethod() === self::METHOD_GET;
    }

    /**
     * Returns request config.
     *
     * @return Config|null
     */
    public function getConfig() {
        return $this->config;
    }

    /**
     * Returns controller key name.
     *
     * @return string
     */
    public function getControllerKey() {
        $key = self::DEFAULT_CONTROLLER_KEY;

        if (isset($this->getConfig()->controllerKey)) {
            $key = $this->getConfig()->controllerKey;
        }
        return $key;
    }

    /**
     * Returns action key name.
     *
     * @return string
     */
    public function getActionKey() {
        $key = self::DEFAULT_ACTION_KEY;

        if (isset($this->getConfig()->actionKey)) {
            $key = $this->getConfig()->controllerKey;
        }
        return $key;
    }

    /**
     * Returns controller name.
     *
     * @return string
     */
    public function getControllerName() {
        return $this->getParam($this->getControllerKey(), $this->getDefaultControllerName());
    }

    /**
     * Set controller name.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setControllerName($name) {
        $this->setParam($this->getControllerKey(), $name);

        return $this;
    }

    /**
     * Returns action name.
     *
     * @return string
     */
    public function getActionName() {
        return $this->getParam($this->getActionKey(), $this->getDefaultActionName());
    }

    /**
     * Set action name.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setActionName($name) {
        $this->setParam($this->getActionKey(), $name);

        return $this;
    }

    /**
     * Returns default controller name.
     *
     * @return Config|mixed|null|string
     */
    public function getDefaultControllerName() {
        $name = self::DEFAULT_CONTROLLER;

        if (isset($this->getConfig()->defaultController)) {
            $name = $this->getConfig()->defaultController;
        }
        return $name;
    }

    /**
     * Returns default action name.
     *
     * @return Config|mixed|null|string
     */
    public function getDefaultActionName() {
        $name = self::DEFAULT_ACTION;

        if (isset($this->getConfig()->defaultAction)) {
            $name = $this->getConfig()->defaultAction;
        }
        return $name;
    }
}
