<?php
class Config implements Countable, Iterator {
    protected $_index;

    protected $_count;

    protected $_data;

    /**
     * Constructor.
     *
     * @param Config|array|string $config
     *
     * @throws Config_Exception
     */
    public function __construct($config) {
        $this->_index = 0;

        if ($config instanceof Config) {
            $config = $config->toArray();
        } elseif (is_string($config)) {
            $config = $this->parseFile($config);
        }

        if (!is_array($config)) {
            throw new Config_Exception('Not supported config type.');
        }

        $this->_data = $config;
        $this->_count = count($this->_data);
    }

    /**
     * Read config file or folder.
     *
     * @param string $fileName file or directory
     *
     * @return array
     */
    protected function parseFile($fileName) {
        $parser = Config_Reader::factory($fileName);

        return $parser->read($fileName);
    }

    /**
     * Magic method.
     *
     * @param string $name
     *
     * @return Config|mixed|null
     */
    public function __get($name) {
        $output = null;
        if (array_key_exists($name, $this->_data)) {
            $output = $this->_data[$name];
        }
        if (is_array($output)) {
            $output = new Config($output);
        }

        return $output;
    }

    /**
     * Magic method.
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return void
     */
    public function __set($name, $value) {
        if ($value instanceof Config) {
            $value = $value->toArray();
        }
        $this->_data[$name] = $value;
        $this->_count = count($this->_data);
        $this->rewind();
    }

    /**
     * Support isset() overloading.
     *
     * @param string $name
     *
     * @return bool
     */
    public function __isset($name) {
        return isset($this->_data[$name]);
    }

    /**
     * Support unset() overloading.
     *
     * @param string $name
     *
     * @return void
     */
    public function __unset($name) {
        if (isset($this->_data[$name])) {
            unset($this->_data[$name]);
            $this->_count = count($this->_data);
            $this->rewind();
        }
    }

    /**
     * Returns an associative array of the stored data.
     *
     * @return array
     */
    public function toArray() {
        return $this->_data;
    }

    /**
     * Defined by Countable interface
     *
     * @return int
     */
    public function count() {
        return $this->_count;
    }

    /**
     * Defined by Iterator interface
     *
     * @return mixed
     */
    public function current() {
        $key = key($this->_data);

        return $this->$key;
    }

    /**
     * Defined by Iterator interface
     *
     * @return mixed
     */
    public function key() {
        return key($this->_data);
    }

    /**
     * Defined by Iterator interface
     *
     * @return mixed
     */
    public function next() {
        next($this->_data);
        ++$this->_index;
    }

    /**
     * Defined by Iterator interface
     *
     * @return mixed
     */
    public function rewind() {
        reset($this->_data);
        $this->_index = 0;
    }

    /**
     * Defined by Iterator interface
     *
     * @return mixed
     */
    public function valid() {
        return $this->_index < $this->_count;
    }
}
