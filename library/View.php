<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright Copyright (c) 2013, Sandor Papp
 * @package   hierarchical_data
 * @license
 */

class View {
    /**
     * Template variable container.
     *
     * @var array
     */
    protected $vars = array();
    /**
     * @var Config|null
     */
    protected $config = null;
    /**
     * @var array
     */
    protected $defaultConfig = array();

    /**
     * Constructor.
     *
     * @param Config $config
     */
    public function __construct(Config $config = null) {
        if (null === $config) {
            $config = new Config($this->defaultConfig);
        }

        $this->config = $config;
    }

    /**
     * Magice method.
     * Supported all application resources.
     *
     * @param string $name
     * @param array $args
     *
     * @return mixed|null
     */
    public function __call($name, $args) {
        $application = Application::getInstance();

        if ('get' == substr($name, 0, 3) and true === $application->hasResource(substr($name, 3))) {
            return $application->getResource(substr($name, 3));
        } elseif ('getConfig' == $name) {
            return $this->config;
        }

        return null;
    }

    /**
     * Set a template variable.
     *
     * @param string $name
     * @param mixed $value
     *
     * @return $this
     */
    public function set($name, $value) {
        $this->vars[$name] = $value;
        return $this;
    }

    /**
     * Returns a template variable.
     *
     * @param string $name
     * @param bool $escape
     *
     * @return null|mixed
     */
    public function get($name, $escape = false) {
        $return = null;
        if (isset($this->vars[$name])) {
            $return = $this->vars[$name];
        }
        if ($return and true === $escape) {
            // FIXME
        }

        return $return;
    }

    /**
     * Render a template.
     *
     * @param string $file
     */
    public function render($file) {
        include $this->getTemplateFilePath($file);
    }

    /**
     * Returns template path.
     *
     * @param string $file
     *
     * @return string
     */
    protected function getTemplateFilePath($file) {
        $filePath = $this->getConfig()->path . DIRECTORY_SEPARATOR;
        $filePath .= $file . '.';
        $filePath .= $this->getConfig()->templateSuffix;

        return $filePath;
    }
}