<?php

class Application {
    /**
     * @var Application
     */
    protected static $instance = null;

    /**
     * @var Config
     */
    protected $config = null;

    /**
     * Application resources container.
     *
     * @var array|null
     */
    protected $resources = null;
    protected $defaultConfig = array(
        'application' => array(
            'path' => ''
            // FIXME
        )
    );

    /**
     * Constructor.
     *
     * @param string|Config $config
     */
    protected function __construct($config) {
        $this->resources = array();

        if (null !== $config) {
            $this->setConfig($config);
        } else {
            $this->setConfig($this->defaultConfig);
        }
    }

    /**
     * Set application config.
     *
     * @param string|array|Config $config
     */
    public function setConfig($config) {
        $this->config = new Config($config);
    }

    /**
     * Returns application config.
     *
     * @return Config|null
     */
    public function getConfig() {
        return $this->config;
    }

    /**
     * Run application.
     */
    public function run() {
        $this->setResource('request', new Request($this->getConfig()->request));
        $this->setResource('layout', new Layout($this->getConfig()->layout));
        $this->setResource('view', new View($this->getConfig()->view));

        if(isset($this->getConfig()->db)){
            Db::getInstance()->addDbAdapters($this->getConfig()->db);
        }

        $controllerName = ucfirst($this->getRequest()->getControllerName()) . 'Controller';

        Loader::loadClass($controllerName, $this->getConfig()->controller->path);

        $this->setResource('controller', new $controllerName($this->getConfig()->controller));
        $this->getController()->dispatch();

    }

    /**
     * Support the $_SERVER superglobal variable access.
     *
     * @param string $name
     *
     * @return mixed|null
     */
    public function getServer($name = null) {
        $return = null;
        if (null !== $name) {
            $name = strtoupper($name);
            if (isset($_SERVER[$name])) {
                $return = $_SERVER[$name];
            }
        } else {
            $return = $_SERVER;
        }

        return $return;
    }

    /**
     * Returns application instance.
     *
     * @param null|string|Config $config
     *
     * @return Application|null
     */
    public static function getInstance($config = null) {
        if (null === self::$instance) {
            self::$instance = new self($config);
        }

        return self::$instance;
    }

    /**
     * Returns true if the resource is exists.
     *
     * @param string $name resource name
     *
     * @return bool
     */
    public function hasResource($name) {
        $name = strtolower($name);

        return isset($this->resources[$name]);
    }

    /**
     * Returns a resource.
     *
     * @param string $name resource name
     *
     * @return mixed
     */
    public function getResource($name) {
        $name = strtolower($name);

        return $this->resources[$name];
    }

    /**
     * Set a resource.
     *
     * @param string $name resiurce name
     * @param mixed $value resource
     *
     * @return $this
     */
    public function setResource($name, $value) {
        $name = strtolower($name);

        $this->resources[$name] = $value;

        return $this;
    }

    /**
     * Magice method.
     *
     * @param string $name
     * @param array $args
     *
     * @return mixed|null
     * @throws Application_Exception
     */
    public function __call($name, $args) {
        if ('get' == substr($name, 0, 3) and true === $this->hasResource(substr($name, 3))) {
            return $this->getResource(substr($name, 3));
        }

        throw new Application_Exception('Not supperted method: ' . $name);
    }
}
