<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright Copyright (c) 2013, Sandor Papp
 * @package   hierarchical_data
 * @license
 */

class Layout extends View {
    /**
     * render types
     */
    const RENDER_TYPE_VIEW = 'view';
    const RENDER_TYPE_JSON = 'json';
    /**
     * @var string current render type
     */
    protected $renderType = 'view';
    /**
     * @var mixed
     */
    protected $data = null;

    /**
     * Render layout.
     *
     * @param null $file
     */
    public function render($file = null) {
        $this->setContentType();

        switch ($this->getRenderType()) {
            case self::RENDER_TYPE_JSON:
                $this->renderJson($this->data);
                break;
            case self::RENDER_TYPE_VIEW:
            default:
                $this->renderView($file);
                break;
        }
    }

    /**
     * Render current view.
     *
     * @return mixed
     */
    public function content() {
        $viewTpl = $this->getRequest()->getControllerName() . DIRECTORY_SEPARATOR . $this->getRequest()->getActionName(
        );

        return $this->getView()->render($viewTpl);
    }

    /**
     * Set layout render type.
     *
     * @param int $type
     *
     * @return $this
     */
    public function setRenderType($type) {
        if (self::RENDER_TYPE_JSON == $type) {
            $this->renderType = $type;
        } else {
            $this->renderType = self::RENDER_TYPE_VIEW;
        }
        return $this;
    }

    /**
     * Returns render type.
     *
     * @return string
     */
    public function getRenderType() {
        return $this->renderType;
    }

    /**
     * Set content type header.
     */
    public function setContentType() {
        switch ($this->getRenderType()) {
            case self::RENDER_TYPE_VIEW:
                $mime = 'text/html';
                break;
            case self::RENDER_TYPE_JSON:
                $mime = 'application/json';
                break;
        }
        header('Content-Type: ' . $mime . '; charset=utf-8');
    }

    /**
     * Set rendered data.
     *
     * @param mixed $data
     */
    public function setData($data) {
        $this->data = $data;
    }

    /**
     * Render by template.
     *
     * @param string $file
     */
    protected function renderView($file) {
        if (null === $file) {
            $file = $this->getConfig()->layout;
        }
        include $this->getTemplateFilePath($file);
    }

    /**
     * Output a json.
     *
     * @param array $data
     */
    protected function renderJson($data) {
        echo json_encode((array)$data);
    }
}