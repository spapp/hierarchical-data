<?php

class Config_Reader {
    const CLASS_PREFIX = __CLASS__;

    /**
     * Constructor.
     */
    protected function __construct() {
    }

    /**
     * Returns parser class name.
     *
     * @param $name
     *
     * @return string
     */
    protected static function getClassName($name) {
        $name = ucfirst(strtolower($name));

        return self::CLASS_PREFIX . '_' . $name;
    }

    /**
     * Returns a config parser class.
     *
     * @param string $name a config file name
     *
     * @return Config_Reader_Abstract
     */
    public static function factory($name) {
        if (is_dir($name)) {
            $reader = 'dir';
        } elseif (is_file($name)) {
            $reader = pathinfo($name, PATHINFO_EXTENSION);
        } else {
            $reader = $name;
        }

        $reader = self::getClassName($reader);

        return new $reader();
    }
}
