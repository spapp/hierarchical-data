<?php
class Config_Reader_Ini extends Config_Reader_Abstract {
    /**
     * @param string $fileName config file
     *
     * @return array
     * @throws Config_Exception
     */
    public function read($fileName) {
        try {
            $array = parse_ini_file($fileName, true);
        } catch (Exception $e) {
            throw new Config_Exception($e->getMessage(), $e->getCode(), $e);
        }

        return $array;
    }
}
