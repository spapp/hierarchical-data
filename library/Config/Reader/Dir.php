<?php
class Config_Reader_Dir extends Config_Reader_Abstract {
    /**
     * @param string $dirName
     *
     * @return array
     * @throws Config_Exception
     */
    public function read($dirName) {
        if (!is_dir($dirName)) {
            throw new Config_Exception('The "' . $dirName . '" is not a folder.');
        } elseif (!is_readable($dirName)) {
            throw new Config_Exception('The "' . $dirName . '" is not exists or not readable.');
        }

        return $this->scan($dirName);
    }

    /**
     * Reads all config file from a folder.
     *
     * @param string $dirName
     *
     * @return array
     */
    protected function scan($dirName) {
        $files = scandir($dirName);
        $array = array();

        foreach ($files as $file) {
            if ('.' != substr($file, 0, 1)) {
                $reader = Config_Reader::factory($dirName . DIRECTORY_SEPARATOR . $file);
                $key = preg_replace('~\.' . $reader . '$~', '', $file);
                $array[$key] = $reader->read($dirName . DIRECTORY_SEPARATOR . $file);
            }
        }

        return $array;
    }
}
