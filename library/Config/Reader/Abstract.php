<?php

abstract class Config_Reader_Abstract {
    /**
     * Constructor.
     */
    public function __construct() {

    }

    /**
     * Magic method.
     * Returns parser type. It is equal the config file extension.
     *
     * @return string
     */
    public function __toString() {
        $name = explode('_', get_class($this));

        return strtolower(array_pop($name));
    }

    /**
     * @param mixed $config
     *
     * @return array
     */
    public abstract function read($config);
}
