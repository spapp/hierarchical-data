<?php
class Config_Reader_Php extends Config_Reader_Abstract {
    /**
     * @param mixed $fileName config file
     *
     * @return array
     * @throws Config_Exception
     */
    public function read($fileName) {
        if (!is_readable($fileName)) {
            throw new Config_Exception('The "' . $fileName . '" is not exists or not readable.');
        }

        $array = include($fileName);

        if (!is_array($array)) {
            throw new Config_Exception('Not valid content ("' . $fileName . '"). Must be an array.');
        }

        return $array;
    }
}
