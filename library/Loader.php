<?php
class Loader {
    const PHP_FILE_SUFFIX = 'php';

    /**
     * Constuctor.
     */
    protected function __constuct() {
    }

    /**
     * Initializate the application autoload function.
     */
    public static function initAutoLoader() {
        if (func_num_args() > 0) {
            forward_static_call_array(array(
                                           'Loader',
                                           'addIncludePaths'
                                      ),
                func_get_args());
        }
        spl_autoload_register('Loader::autoLoad');
    }

    /**
     * Add some folder to php include path list.
     *
     * @params string
     *
     * @return void
     */
    public static function addIncludePaths() {
        if (func_num_args() > 0) {
            $path = implode(PATH_SEPARATOR, func_get_args());
            set_include_path(implode(PATH_SEPARATOR,
                array(
                     $path,
                     get_include_path(),
                )));
        }
    }

    /**
     * Load a class.
     *
     * @param string $className class or interface name
     * @param array $dirs
     * @param bool $once
     *
     * @return bool
     * @throws Loader_Exception
     */
    public static function loadClass($className, $dirs = null, $once = true) {
        $fileName = self::getFilePath($className);
        $oldIncludePath = null;

        if (null !== $dirs) {
            $dirs = (array)$dirs;
            $oldIncludePath = get_include_path();
            forward_static_call_array(array(
                                           'Loader',
                                           'addIncludePaths'
                                      ),
                $dirs);
        }

        if (false === self::isFile($fileName)) {
            throw new Loader_Exception('The "' . $className . '" is not exists or not readable.');
        }

        if (true === $once) {
            include_once $fileName;
        } else {
            include $fileName;
        }

        if (!class_exists($className, false) && !interface_exists($className, false)) {
            throw new Loader_Exception('The "' . $className . '" was not found.');
        }

        if (null !== $oldIncludePath) {
            set_include_path($oldIncludePath);
        }

        return true;
    }

    /**
     * The file is exists and readable each include path.
     *
     * @param string $fileName
     *
     * @return bool
     */
    public static function isFile($fileName) {
        $paths = explode(PATH_SEPARATOR, get_include_path());

        foreach ($paths as $path) {
            $file = $path . DIRECTORY_SEPARATOR . $fileName;
            if (is_readable($file)) {
                return true;
            }
        }

        return false;
    }

    /**
     * The application autoload function.
     *
     * @param string $className class or interface name
     */
    public static function autoLoad($className) {
        self::loadClass($className);
    }

    /**
     * @param $name
     *
     * @return string
     */
    public static function getFilePath($name) {
        return str_replace('_', DIRECTORY_SEPARATOR, $name) . '.' . self::PHP_FILE_SUFFIX;
    }
}
