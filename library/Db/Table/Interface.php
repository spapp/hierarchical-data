<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright Copyright (c) 2013, Sandor Papp
 * @package   hierarchical_data
 * @license   http://creativecommons.org/licenses/by-nc-nd/3.0/deed.hu
 */

interface Db_Table_Interface {
    /**
     * Set database adapter.
     *
     * @param Db_Adapter_Abstract $adapter
     *
     * @return $this
     */
    public function setAdapter(Db_Adapter_Abstract $adapter);

    /**
     * Returns database adapter.
     *
     * @return Db_Adapter_Abstract|null
     */
    public function getAdapter();

    /**
     * Insert new records in the table.
     *
     * @param array $data
     *
     * @return int last insert id
     */
    public function insert(array $data);

    /**
     * Update records in the table.
     *
     * @param array $data
     * @param array|string $where
     *
     * @return mixed
     */
    public function update(array $data, $where = null);

    /**
     * Delete records in the table.
     *
     * @param sreing|array $where
     *
     * @return mixed
     */
    public function delete($where);

    /**
     * Sselect data from the table.
     *
     * @param string|array $where
     * @param string|array $columns
     *
     * @return mixed
     */
    public function select($where = null, $columns = '*');
}