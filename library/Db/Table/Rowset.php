<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright Copyright (c) 2013, Sandor Papp
 * @package   hierarchical_data
 * @license   http://creativecommons.org/licenses/by-nc-nd/3.0/deed.hu
 */
class Db_Table_Rowset implements Countable, Iterator {
    /**
     * @var int
     */
    protected $index;
    /**
     * @var int
     */
    protected $count;
    /**
     * @var resource
     */
    protected $resource;
    /**
     * @var Db_Adapter_Abstract
     */
    protected $adapter;
    /**
     * @var Db_Table
     */
    protected $table;

    /**
     * Constructor.
     *
     * @param array $config
     */
    public function __construct(array $config) {
        $this->resource = $config['resource'];
        $this->adapter = $config['adapter'];
        $this->table = $config['table'];

        $this->index = 0;
        $this->count = $this->adapter->getCount($this->resource);
    }

    public function toArray() {
        $array = array();

        foreach ($this as $row) {
            array_push($array, $row->toArray());
        }

        return $array;
    }

    public function getRowClass() {
        return $this->table->getRowClass();
    }

    /**
     * Defined by Countable interface
     *
     * @return int
     */
    public function count() {
        return $this->count;
    }

    /**
     * Defined by Iterator interface
     *
     * @return mixed
     */
    public function current() {
        $this->getAdapter()->seek($this->getResource(), $this->index);
        $current = $this->getAdapter()->getNextRecord($this->getResource());
        $rowClass = $this->getRowClass();

        return new $rowClass(array(
                                  'data' => $current,
                                  'table' => $this->getTable()
                             ));
    }

    /**
     * Defined by Iterator interface
     *
     * @return mixed
     */
    public function key() {
        return $this->index;
    }

    /**
     * Defined by Iterator interface
     *
     * @return mixed
     */
    public function next() {
        ++$this->index;
    }

    /**
     * Defined by Iterator interface
     *
     * @return mixed
     */
    public function rewind() {
        $this->index = 0;
        $this->getAdapter()->seek($this->getResource(), $this->index);
    }

    /**
     * Defined by Iterator interface
     *
     * @return mixed
     */
    public function valid() {
        return $this->index < $this->count;
    }

    /**
     * @return Db_Adapter_Abstract
     */
    protected function getAdapter() {
        return $this->adapter;
    }

    /**
     * @return resource
     */
    protected function getResource() {
        return $this->resource;
    }

    /**
     * @return Db_Table
     */
    protected function getTable() {
        return $this->table;
    }
}



