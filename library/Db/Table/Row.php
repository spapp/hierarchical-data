<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright Copyright (c) 2013, Sandor Papp
 * @package   hierarchical_data
 * @license   http://creativecommons.org/licenses/by-nc-nd/3.0/deed.hu
 */

class Db_Table_Row {
    // todo implements Countable, Iterator
    /**
     * @var array
     */
    protected $data;
    /**
     * @var Db_Table
     */
    protected $table;

    /**
     * Constructor.
     *
     * @param array $config
     */
    public function __construct(array $config) {
        $this->data = $config['data'];
        $this->table = $config['table'];
    }

    /**
     * Magice method.
     *
     * @param string $name
     *
     * @return mixed|null
     */
    public function __get($name) {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }
        return null;
    }

    /**
     * Magice method.
     *
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value) {
        $this->data[$name] = $value;
    }

    /**
     * Magice method.
     *
     * @param string $name
     *
     * @return bool
     */
    public function __isset($name) {
        return isset($this->data[$name]);
    }

    /**
     * @return Db_Table
     */
    public function getTable() {
        return $this->table;
    }

    /**
     * @return array
     */
    public function toArray() {
        return $this->data;
    }

    /**
     * Delete current row from the table.
     */
    public function delete() {
        // fixme get primary key
        $this->getTable()->delete('id = ' . $this->id);
    }

    /**
     * Save current row to the table.
     *
     * @return int|mixed
     */
    public function save() {
        if (isset($this->id)) {
            return $this->getTable()->update($this->toArray(), 'id = ' . $this->id);
        } else {
            return $this->getTable()->insert($this->toArray());
        }
    }
}
