<?php
/**
 * Class Db_Adapter_Mysql.
 * The mysql extension is deprecated as of PHP 5.5.0.
 *
 * @see http://www.php.net/manual/en/intro.mysql.php
 */
class Db_Adapter_Mysql extends Db_Adapter_Abstract {
    protected $defaultConfig = array(
        'host' => 'localhost',
        'port' => 3306,
        'charset' => 'utf8'
    );

    /**
     * Magice function.
     * Supported adapter config with 'get' prefix. For example: getHost.
     *
     * @param string $name
     * @param array $args
     *
     * @return Config|mixed|null
     * @throws Db_Exception
     */
    public function __call($name, $args) {
        $return = null;

        if ('get' == substr($name, 0, 3)) {
            $name = strtolower(substr($name, 3));
            if (isset($this->getConfig()->$name)) {
                $return = $this->getConfig()->$name;
            } elseif (isset($this->defaultConfig[$name])) {
                $return = $this->defaultConfig[$name];
            }
        } else {
            throw new Db_Exception('Not supported method.');
        }

        return $return;
    }

    /**
     * Defined in Db_Adapter_Abstract.
     * Connect to database.
     *
     * @return bool|resource
     * @throws Db_Exception
     */
    public function connect() {
        // FIXME use try-catch
        $this->connection = mysql_connect(
            $this->getHost() . ':' . $this->getPort(),
            $this->getUsername(),
            $this->getPassword()
        );

        if (!$this->getConnection()) {
            throw new Db_Exception(mysql_error(), mysql_errno());
        }

        mysql_set_charset($this->getCharset(), $this->getConnection());

        if (!mysql_select_db($this->getDbname(), $this->getConnection())) {
            throw new Db_Exception(mysql_error(), mysql_errno());
        }

        return true;
    }

    /**
     * Defined in Db_Adapter_Abstract.
     * Close current database conection.
     *
     * @return void
     */
    public function close() {
        if ($this->getConnection()) {
            mysql_close($this->getConnection());
        }
    }

    /**
     * Defined in Db_Adapter_Abstract.
     * Escaping a string.
     *
     * @param string $string
     *
     * @return string
     */
    public function escape($string) {
        return mysql_real_escape_string((string)$string, $this->getConnection());
    }

    /**
     * Defined in Db_Adapter_Abstract.
     *
     * @param string $query SQL query
     * @param array $params
     *
     * @return mixed|resource
     */
    public function query($query, $params = array()) {
        $query = $this->prepare(trim($query), $params);
        $resurce = mysql_query($query, $this->getConnection());

        if (preg_match('~^INSERT~i', trim($query))) {
            $resurce = mysql_insert_id($this->getConnection());
        }

        return $resurce;
    }

    /**
     * Get number of rows in result.
     *
     * @param resource $resource
     *
     * @return int
     */
    public function getCount($resource) {
        return mysql_num_rows($resource);
    }

    /**
     * Fetch a result row as an associative array.
     *
     * @param resource $resource
     *
     * @return array
     */
    public function getNextRecord($resource) {
        return mysql_fetch_assoc($resource);
    }

    /**
     * Move internal result pointer.
     *
     * @param resource $resource
     * @param int $rowNumber
     *
     * @return bool
     */
    public function seek($resource, $rowNumber) {
        return mysql_data_seek($resource, $rowNumber);
    }
}
