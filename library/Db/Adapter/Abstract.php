<?php

abstract class Db_Adapter_Abstract {
    /**
     * @var Config
     */
    protected $config = null;
    /**
     * @var null
     */
    protected $connection = null;

    /**
     * Constructor.
     */
    public function __construct($config) {
        $this->setConfig($config);
        $this->connect();
    }

    /**
     * Destructor.
     */
    public function __destruct() {
        $this->close();
    }

    /**
     * Set the adapter config.
     *
     * @param array|Config $config
     *
     * @throws Db_Exception
     */
    public function setConfig($config) {
        if (is_array($config)) {
            $this->config = new Config($config);
        } elseif (!($config instanceof Config)) {
            throw new Db_Exception('Invalid config format');
        }
    }

    /**
     * Returns adapter config.
     *
     * @return Config|null
     */
    public function getConfig() {
        return $this->config;
    }

    /**
     * Returns database conection link identifier.
     *
     * @return null|resource
     */
    public function getConnection() {
        return $this->connection;
    }

    /**
     * @param string $string
     * @param array $params
     *
     * @return mixed
     */
    public function prepare($string, array $params) {
        foreach ($params as $name => $value) {
            $string = str_replace(Db::PARAM_TOKEN . $name, "'" . $this->escape($value) . "'", $string);
        }

        return $string;
    }

    /**
     * Connect to database.
     *
     * @return resource
     */
    public abstract function connect();

    /**
     * Close current database conection.
     *
     * @return void
     */
    public abstract function close();

    /**
     * Escaping a string.
     *
     * @param string $string
     *
     * @return string
     */
    public abstract function escape($string);

    /**
     * @param string $query SQL query
     * @param array $params
     *
     * @return mixed
     */
    public abstract function query($query, $params = array());

    /**
     * Get number of rows in result.
     *
     * @param resource $resource
     *
     * @return int
     */
    public abstract function getCount($resource);

    /**
     * Fetch a result row as an associative array.
     *
     * @param resource $resource
     *
     * @return array
     */
    public abstract function getNextRecord($resource);

    /**
     * Move internal result pointer.
     *
     * @param resource $resource
     * @param int $rowNumber
     *
     * @return bool
     */
    public abstract function seek($resource, $rowNumber);

}
