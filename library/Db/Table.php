<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright Copyright (c) 2013, Sandor Papp
 * @package   hierarchical_data
 * @license   http://creativecommons.org/licenses/by-nc-nd/3.0/deed.hu
 */

class Db_Table implements Db_Table_Interface {
    /**
     * @var string table name
     */
    protected $name = null;
    /**
     * @var null|Db_Adapter_Abstract
     */
    protected $adapter = null;
    /**
     * @var string
     */
    protected $rowClass = 'Db_Table_Row';

    /**
     * Constructor.
     *
     * @param string $name
     * @param Db_Adapter_Abstract $adapter
     */
    public function __construct($name = null, Db_Adapter_Abstract $adapter = null) {
        if (null !== $name) {
            $this->name = (string)$name;
        }

        if (null !== $adapter) {
            $this->setAdapter($adapter);
        }
    }

    /**
     * Set database adapter.
     *
     * @param Db_Adapter_Abstract $adapter
     *
     * @return $this
     */
    public function setAdapter(Db_Adapter_Abstract $adapter) {
        $this->adapter = $adapter;

        return $this;
    }

    /**
     * Returns database adapter.
     *
     * @return Db_Adapter_Abstract|null
     */
    public function getAdapter() {
        if (null === $this->adapter) {
            return Db::getDefaultAdapter();
        }
        return $this->adapter;
    }

    /**
     * Insert new records in the table.
     *
     * @param array $data
     *
     * @return int last insert id
     */
    public function insert(array $data) {
        $columns = array_keys($data);
        $tokens = array();
        $query = array(
            'INSERT INTO',
            $this->name
        );

        foreach ($columns as $column) {
            array_push($tokens, Db::PARAM_TOKEN . $column);
        }

        array_push($query, '(', implode(', ', $columns), ')');
        array_push($query, 'VALUES', '(', implode(', ', $tokens), ')');

        return $this->query(implode(' ', $query), $data);

    }

    /**
     * Update records in the table.
     *
     * @param array $data
     * @param array|string $where
     *
     * @return mixed
     */
    public function update(array $data, $where = null) {
        $columns = array();
        $query = array(
            'UPDATE',
            $this->name,
            'SET'
        );

        foreach ($data as $column => $value) {
            array_push($columns, $column . ' = ' . Db::PARAM_TOKEN . $column);
        }

        array_push($query, implode(', ', $columns));

        if (null !== $where) {
            array_push($query, 'WHERE', $this->prepareWhere($where));
        }

        return $this->query(implode(' ', $query), $data);
    }

    /**
     * Delete records in the table.
     *
     * @param sreing|array $where
     *
     * @return mixed
     */
    public function delete($where) {
        $query = array(
            'DELETE FROM',
            $this->name,
            'WHERE',
            $this->prepareWhere($where)
        );

        return $this->query(implode(' ', $query));
    }

    /**
     * Sselect data from the table.
     *
     * @param string|array $where
     * @param string|array $columns
     * @param array $params
     *
     * @return mixed
     */
    public function select($where = null, $columns = '*', $params = array()) {
        $query = array(
            'SELECT',
            implode(', ', (array)$columns),
            'FROM',
            $this->name
        );

        if (null !== $where) {
            array_push($query, 'WHERE', $this->prepareWhere($where));
        }

        return $this->query(implode(' ', $query), $params);
    }

    /**
     * Defined in Db_Adapter_Abstract.
     *
     * @param string $query SQL query
     * @param array $params
     *
     * @return mixed|Db_Table_Rowset
     */
    public function query($query, $params = array()) {
        $resource = $this->getAdapter()->query($query, $params);

        if (preg_match('~^SELECT~i', trim($query))) {
            $resource = new Db_Table_Rowset(array(
                                                 'resource' => $resource,
                                                 'adapter' => $this->getAdapter(),
                                                 'table' => $this
                                            ));
        }
        return $resource;
    }

    /**
     * Returns table row class name.
     *
     * @return string
     */
    public function getRowClass() {
        return $this->rowClass;
    }

    /**
     * Returns a row model.
     *
     * @param array $data
     *
     * @return mixed|Db_Table_Row
     */
    public function createRow($data = array()) {
        $rowClass = $this->getRowClass();
        return new $rowClass(array(
                                  'data' => $data,
                                  'table' => $this
                             ));
    }

    /**
     * Prepare SQL where statement.
     *
     * @todo support some formats
     *
     * @param $where
     *
     * @return string
     */
    protected function prepareWhere($where) {
        // FIXME where -> class Where, formats
        return implode(' AND ', (array)$where);
    }
}
