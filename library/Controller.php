<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright Copyright (c) 2013, Sandor Papp
 * @package   hierarchical_data
 * @license
 */

class Controller {
    /**
     * @var Config|null
     */
    protected $config = null;
    /**
     * @var array
     */
    protected $defaultConfig = array(
        'defaultAction' => 'default'
    );

    /**
     * Constructor.
     *
     * @param Config $config
     */
    public function __construct(Config $config = null) {
        if (null === $config) {
            $config = new Config($this->defaultConfig);
        }

        $this->config = $config;

        $this->init();

        $actionFn = $this->getRequest()->getActionName($this->getConfig()->defaultAction) . 'Action';

        if (method_exists($this, $actionFn)) {
            $this->{$actionFn}();
        } else {

        }
    }

    /**
     * Controller inicialization.
     */
    public function init() {

    }

    /**
     * Dispatch.
     */
    public function dispatch() {
        $this->getLayout()->render();
    }

    /**
     * Set rendered data.
     *
     * @param mixed $data
     */
    public function setData($data) {
        $this->getLayout()->setData($data);
    }

    /**
     * Magice method.
     * Supported all application resources.
     *
     * @param string $name
     * @param array $args
     *
     * @return mixed|null
     * @throws Controller_Exception
     */
    public function __call($name, $args) {
        $application = Application::getInstance();

        if ('get' == substr($name, 0, 3) and true === $application->hasResource(substr($name, 3))) {
            return $application->getResource(substr($name, 3));
        }

        throw new Controller_Exception('Not supported method: "' . $name . '"');
    }

    /**
     * Returns controller config.
     *
     * @return Config|null
     */
    public function getConfig() {
        return $this->config;
    }

    /**
     * Returns a mapper class.
     *
     * @param string $name
     *
     * @return mixed
     */
    public function getMapper($name) {
        return forward_static_call_array(
            array(
                 'Model_Mapper_' . ucfirst($name),
                 'getInstance'
            ), array()
        );
    }
}